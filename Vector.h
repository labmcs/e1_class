#ifndef VECTOR_H
#define VECTOR_H

#include<iostream>
#include<cmath>

class Vector{
    public:
        Vector(double x=0, double y=0, double z=0): m_v{x,y,z}{};

        inline double X(void) const { return m_v[0]; };
        inline double Y(void) const { return m_v[1]; };
        inline double Z(void) const { return m_v[2]; };
        inline void X(double x){ m_v[0] = x; }  
        inline void Y(double y){ m_v[1] = y; } 
        inline void Z(double z){ m_v[2] = z; }        

        double Abs() const;
        Vector Unit() const;

        Vector operator+(const Vector&) const;
        Vector operator-() const;
        Vector operator-(const Vector&) const;
        Vector operator*(double a) const ;
        double operator*(const Vector&) const;
        Vector operator/(double) const;
        Vector operator^(const Vector&) const;
        bool operator==(const Vector&) const;
        bool operator!=(const Vector&) const;
        bool operator<(const Vector&) const;

        friend std::ostream& operator<<(std::ostream&, const Vector&);
        friend std::istream& operator>>(std::istream&, Vector&);

    private:
        double m_v[3];
        mutable double m_abs = 0;
};

inline Vector operator*(double a, const Vector& v) { return v*a; }

#endif

