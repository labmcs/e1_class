
CC=gcc
CPP=g++
CFLAGS=-Wall -g --std=c++11
ROOTLIB=`root-config --glibs`
ROOTFLAG=`root-config --clfags`

OBJ=Vector.o MatPoint.o

all: main

main: main.cxx $(OBJ)
	$(CPP) $(CFLAGS) main.cxx $(OBJ)

%.o: %.cxx
	$(CPP) $(CFLAGS) -c $<

.PHONY: clean

clean:
	rm -rf $(OBJ) a.out a.out.dSYM

