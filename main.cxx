//#include "Electron.h"
#include "MatPoint.h"
#include <iostream>

#include<fstream>
#include<vector>

/* using namespace std; */

int main(){

    std::vector<MatPoint> m;

    std::ifstream data("effemeridi_filtered");

    MatPoint mr;
    
    for(int i=0; data >> mr; i++)
        m.push_back(mr);

    for(auto o: m)
        std::cout << o << "\n";

    return 0;
   
/*   //Costruzione da particella "nota" */
/*   MatPoint p(Electron(),Vector(1,0,0)); */
/*   cout << p.Mass() << " " << p.Charge() << endl; */
/*   cout << p.R() << endl; */
/*   cout << p.V() << endl; */
/*   //Costruzione punto materiale generico */
/*   MatPoint pm(1,0,Vector(1,0,0),Vector(1,1,1)); */
/*   cout << pm.Mass() << " " << pm.Charge() << endl; */
/*   cout << pm.R() << endl; */
/*   cout << pm.V() << endl; */
/*   // Calcolo Campo Gravitazionale */
/*   Vector v = pm.GravField(Vector(1.5,0,0)); */
/*   if (v.X()!=-4){ */
/*     cout << "Errore campo vettoriale:" << endl; */
/*     cout << " valore aspettato -4, ottenuto " << v.X() << endl; */
/*   } */

  
}
