/* 
 * point class implementation
 * Author: Mattia Sotgia
 * Date: 13/10/2022
*/

#include"MatPoint.h"
#include<cmath>
#include<string>

Vector MatPoint::GravField(const Vector& position) const {
    return Vector( -(position-m_coord).Unit() * m_mass/std::pow((position-m_coord).Abs(), 2) );
}

std::istream& operator>>(std::istream& i, MatPoint& m){
    std::string name;
    double mass, charge, x, y, z, vx, vy, vz;
    i >> name >> mass >> charge >> x >> vx >> y >> vy >> z >> vz;
    m.m_name=name;
    m.m_coord=Vector(x,y,z);
    m.m_speed=Vector(vx,vy,vz);
    m.m_mass=mass;
    m.m_charge=charge;
    return i;
}

std::ostream& operator<<(std::ostream& o, const MatPoint& m){
    o << m.m_name << " => R: " << m.m_coord << ", V: " << m.m_speed << ", mass: " << m.m_mass << " [arb.u.], charge: " << m.m_charge << " coulomb "; 
    return o;
}
