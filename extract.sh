#!/bin/bash

OUTPUT=effemeridi_filtered

echo " " > $OUTPUT
for planet in Sun Mercury Venus Earth Moon Mars Jupiter Saturn Uranus Neptune Pluto
do
    echo $planet >> $OUTPUT
    grep "/\* $planet" effemeridi.h | awk '{print $1}' >> $OUTPUT #print PLANET MASS*G
    echo "0" >> $OUTPUT
    grep -A6 "// $planet" effemeridi.h | grep -v "// $planet" >> $OUTPUT
    echo " " >> $OUTPUT
done


