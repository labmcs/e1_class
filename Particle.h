#ifndef PARTICLE_H
#define PARTICLE_H

#include<string>
#include"Particle.h"

#define E 1.60217663e-19

class Particle{
    public: 
        Particle(double mass = 0, double charge = 0, std::string name = "generic"): m_mass(mass), m_charge(charge), m_name(name) {};

        inline double   Charge()    const { return m_charge; };
        inline void     Charge(double charge) { m_charge = charge; };
        inline double   Mass()      const {return m_mass; };
        inline void     Mass(double mass) { m_mass = mass; };

    protected:

        double m_mass = -1;
        double m_charge = 0;
        std::string m_name;

};

class Electron: public Particle{
    public:
        Electron(): Particle(9.1093837e-31, -E, "Electron"){};
};

class Proton: public Particle{
    public:
        Proton(): Particle(1.67262192e-27, E, "Proton"){};
};

#endif

