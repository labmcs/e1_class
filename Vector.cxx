/*
 * Vector class implementation
 * Author: Mattia Sotgia
 * Date: 09/10/2022
*/


#include"Vector.h"

////////////////////////////////// METHODS
//////////////////////////////////////////

double Vector::Abs() const { 
    double s = 0;
    for(int i=0; i<3; i++)
        s+=m_v[i]*m_v[i];
    m_abs = std::sqrt( s );
    return m_abs;
};

Vector Vector::Unit() const { 
    return Vector( (*this)/this->Abs() ); 
};

//////////////////////////////// OPERATORS
//////////////////////////////////////////

Vector Vector::operator+(const Vector& v) const {
    Vector s;
    for(int i=0; i<3; i++)
        s.m_v[i] = m_v[i] + v.m_v[i];
    return s;
};
 
Vector Vector::operator-() const {
    Vector m;
    for(int i=0; i<3; i++)
        m.m_v[i] = -m_v[i];
    return m;
};

Vector Vector::operator-(const Vector& v) const {
    return Vector( (*this) + (-v) );
};


Vector Vector::operator*(double a) const {
    Vector p;
    for(int i=0; i<3; i++)
        p.m_v[i] = a*m_v[i];

    return p;
};

double Vector::operator*(const Vector& v) const {
    double s = 0;
    for(int i=0; i<3; i++)
        s += m_v[i] * v.m_v[i];
    return s;
};

Vector Vector::operator/(double a) const {
    return Vector( (*this)*(1/a)); 
};

Vector Vector::operator^(const Vector& v) const {
    return Vector(
            m_v[1]*v.m_v[2] - m_v[2]*v.m_v[1],
            m_v[2]*v.m_v[0] - m_v[0]*v.m_v[2],
            m_v[0]*v.m_v[1] - m_v[1]*v.m_v[0] 
            );
};

bool Vector::operator==(const Vector& v) const {
    return m_v[0]==v.m_v[0] 
        && m_v[1]==v.m_v[1] 
        && m_v[2]==v.m_v[2]; 
}

bool Vector::operator!=(const Vector& v) const { 
    return m_v[0]!=v.m_v[0] || m_v[1]!=v.m_v[1] || m_v[2]!=v.m_v[2]; 
}

bool Vector::operator<(const Vector& v) const { 
    return this->Abs()<v.Abs(); 
};

//////////////////////////////// STREAMERS
//////////////////////////////////////////

std::ostream& operator<<(std::ostream& o, const Vector& v){
    o << "(";
    for(int i=0; i<3; i++)
        o << v.m_v[i] << ((i==2)?")":", ");
    return o;
};

std::istream& operator>>(std::istream& i, Vector& v){
    for(int j=0; j<3; j++){
        double e;
        i >> e; 
        v.m_v[j] = e;
    }
    return i;
};

