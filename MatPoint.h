#ifndef MPOINT_H
#define MPOINT_H

#include<iostream>
#include<cmath>

#include"Vector.h"
#include"Particle.h"

class MatPoint: public Particle{
    public:
        MatPoint(const Particle& particle=Particle(), const Vector& coordinates=Vector(), const Vector& speed=Vector()): 
            Particle(particle), m_coord(coordinates), m_speed(speed){};
        MatPoint(double mass, double charge, const  Vector& coordinates, const Vector& speed): Particle(mass, charge), m_coord(coordinates), m_speed(speed){};

        inline Vector R() const { return m_coord; };
        inline void R(double x, double y, double z) { m_coord.X(x); m_coord.Y(y); m_coord.Z(z); };
        inline void R(Vector& coordinates) { m_coord = coordinates; };
        inline Vector V() const {return m_speed; };
        inline void V(Vector speed) { m_speed = speed; };

        Vector GravField(const Vector&) const;
        friend std::istream& operator>>(std::istream&, MatPoint&);
        friend std::ostream& operator<<(std::ostream&, const MatPoint&);
    private:

        Vector m_coord;
        Vector m_speed;
};

#endif

