# Scaricate le tracce di partenza da git

`git clone https://github.com/fabrizio-parodi/LabMCS-EsI.git EsI`

## Prima parte
Progettazione della gerarchia di classi per la descrizione di un punto materiale

 - `Particle` (oggetto con massa e carica)
 - `MatPoint` (particella con posizione e velocità) avvalendosi anche della classe Vector (necessaria per la descrizione di vettori 3D).
Implementazione di header file (.h) e file sorgente (.cpp) (in alcuni di questi casi potete limitarvi al .h sfruttando metodi inline).

Aggiungete poi le seguenti funzionalità:

 - create un metodo `MatPoint::GravField` che calcoli il campo gravitazionale creato dall'oggetto in un punto generico dello spazio (descritto da un `Vector`) ritornando il risultato in un `Vector`
 - derivare una specializzazione di `Particle`: la classe Electron (a scelta aggiungete anche Proton e/o prevedete che la classe Particle possa contenere anche il suo nome (utile nel caso di corpi celesti)).
 - Mantenere il nome indicato per le classi e, come metodi di accesso, Mass, Charge, R, V (per posizione e velocità).
 - Verificate il tutto compilando con make (estendete il `Makefile` fornito) il sorgente `main.cxx` (disegnato per testare la classe).

## Seconda parte
Filtrate il file `SolarSysData.h` estendendo lo script extract.sh creato a lezione con la seguente funzionalità

 - estrazione del valore della massa del pianeta
 - Il file di uscita dovrà avere il seguente formato
```log
Mass_1
Coordinates_1
Mass_2
Coordinates_2
...
Mass_N
Coordinates_N
```
(se nella classe particella avete previsto anche il nome del corpo dovrete ovviamente salvare anche il nome del pianeta)



## Compito per casa
Affrontate, a scelta, uno dei seguenti problemi
 - Create un classe di numeri complessi derivandola da TVector2 (classe di ROOT per vettori 2D). La classe deve avere: la ridefinizione di << che stampi opportunamente il numero complesso, un metodo per il complesso coniugato, gli operatori +,-,* (per alcuni di questi, ma non per tutti, vanno bene quelli default derivati da TVector2). Create un breve programma di test.
 - Create una classe FIFO (first in first out) derivandola da vector<double>. Il costruttore dovrà fissare la lunghezza e il metodo che aggiunge un valore dovrà fare in modo che quando l'array è pieno, all'aggiunta di un nuovo elemento, venga eliminato il valore più vecchio (aiuto: per accede a vector dentro la classe derivata usate this)
 - Partendo dall'esempio visto in classe creare una classe poligono. Definite l'area come un metodo puramente virtual (e quindi astratto). Implementate il metodo perimetro in poligono, derivate la classe triangolo e implementate area in triangolo. Si usi TVector2 per descrivere i vertici del poligono. Create un breve programma di test.

